package de.bund.bva.pliscommon.pliswebgui.gui.jsfvorlagen.jsfsteuerelemente;

import de.bund.bva.isyfact.common.web.jsf.components.datatable.DataTableModel;

/**
 * Model für die Trefferliste.
 *
 * @author Capgemini, Tobias Groeger
 * @version $Id: JsfSteuerelementeTrefferlistenModel.java 130053 2015-02-10 12:46:06Z sdm_tgroeger $
 */
public class JsfSteuerelementeTrefferlistenModel extends DataTableModel<JsfSteuerelementeTreffer> {

    /**
     * Die Serial-Version UID.
     */
    private static final long serialVersionUID = 1L;

}
