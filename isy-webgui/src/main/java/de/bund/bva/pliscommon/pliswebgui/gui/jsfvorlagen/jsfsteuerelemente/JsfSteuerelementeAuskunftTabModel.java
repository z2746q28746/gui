package de.bund.bva.pliscommon.pliswebgui.gui.jsfvorlagen.jsfsteuerelemente;

import java.util.List;
import com.google.common.collect.Lists;
import de.bund.bva.isyfact.common.web.jsf.components.tab.TabGroupModel;

/**
 * Model für das Tab-Demobeispiel (Vollauskunft, Personalien, Sachverhalte)
 *
 */
public class JsfSteuerelementeAuskunftTabModel extends TabGroupModel {

    /**
     * Die Serial-Version UID.
     */
    private static final long serialVersionUID = 1L;

}