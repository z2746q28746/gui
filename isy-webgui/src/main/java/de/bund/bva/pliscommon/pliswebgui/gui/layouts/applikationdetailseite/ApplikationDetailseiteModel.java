package de.bund.bva.pliscommon.pliswebgui.gui.layouts.applikationdetailseite;

import de.bund.bva.isyfact.common.web.global.AbstractMaskenModel;

/**
 * Model für ApplikationDetailseite.
 *
 * @author Capgemini, Tobias Groeger
 * @version $Id: ApplikationDetailseiteModel.java 130053 2015-02-10 12:46:06Z sdm_tgroeger $
 */
public class ApplikationDetailseiteModel extends AbstractMaskenModel {

    /**
     * Die Serial-Version UID.
     */
    private static final long serialVersionUID = 1L;

}
